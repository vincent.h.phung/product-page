AOS.init({
  offset: 400, // offset (in px) from the original trigger point
  delay: 0, // values from 0 to 3000, with step 50ms
  duration: 1000, // values from 0 to 3000, with step 50ms
});

// ANIMATIONS

const tl = gsap.timeline({ defaults: { duration: 1.2 } });
const tl2 = gsap.timeline({
  scrollTrigger: {
    trigger: ".about-text",
    start: "center bottom",
  },
});

tl.from(".animate", { opacity: 0, y: 40, stagger: 0.4 });
tl.from(".main-head", { opacity: 0, y: "-100%" }, "-=1");
tl.from(".intro-img", { opacity: 0, y: 40 }, "-=1");

tl2.from(".about-text", { opacity: 0, y: 40, duration: 1 });

// HAMBURGER MENU

const hamburger = document.querySelector(".hamburger");
const navLinks = document.querySelector(".nav_links");
const links = document.querySelectorAll(".nav_links li");
const introText = document.querySelector(".intro-text");

hamburger.addEventListener("click", () => {
  navLinks.classList.toggle("nav-open");
  document.body.classList.toggle("hide");
  document.querySelector("html").classList.toggle("hide");
});
links.forEach((link) => {
  link.addEventListener("click", () => {
    navLinks.classList.toggle("nav-open");
    document.body.classList.remove("hide");
    document.querySelector("html").classList.remove("hide");
  });
});

const marquee = document.querySelector(".marquee");
const marqueeContent = document.querySelector(".marquee").innerHTML;
marquee.innerHTML = marqueeContent + marqueeContent;
